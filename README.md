#Junior JavaScript Angular.js developer - test
It's test task for the position of the Junior JavaScript developer (Angular)

Тестовое задание для AngularJS-разработчика

Реализуйте single-page application, состоящее из двух экранов-страниц.

На индексной странице расположите таблицу со списком задач. Cписок получите из файла tasks.json.

В таблице нужно вывести только активные задачи (поле ‘obj_status’ имеет значение ‘active’);
Кроме имени задачи также выведите теги (поле ‘tags’), время потраченное на задачу (‘actual_effort’), общую оценку задачи (‘estimated_effort’), и дату окончания задачи (‘due_date’) в каком-нибудь человеко-понятном формате;
Название важных задач (‘is_high_priority’) должны выделяться (жирным/другим цветом).

При клике на название задачи в таблице пользователь должен переходить на страницу задачи.

На странице задачи выведите дополнительную информацию о задаче (например description);
при клике на название задачи оно должно редактироваться (inlineeditor) и после сохранения задача должна отправляться на сервер. Сервера нет, поэтому можно просто сделать PUT-запрос на любой адрес.

Должны быть использованы сервисы Angular, такие как $http/$resource, $route, promises, контроллеры, директивы.

Код должен быть покрыт юнит тестами в степени, которая кажется вам разумной.

Ссылка на пример tasks.json
https://docs.google.com/document/d/1YWYIP1_1mL3qWjBKaueWbvwfGB54AOD0aj2Rt6MQdNM/edit?usp=sharing

##Quick start

* Install dev-dependencies `npm i`
* Install dependencies `bower i`
* Launch `gulp` to run watchers, server, compilers and build

##Directory Layout

	interact                    # Project root
	├── /build/                 # Minified files for production 50/50 ))
	├── /src/                   # Source files for developing
	├── bower.json              # List of 3rd party libraries and utilities
	├── package.json            # Dependencies for node.js
	├── gulpfile.js             # gulp.js config
	├── LICENSE                 # License file
	├── README.md               # File you read
