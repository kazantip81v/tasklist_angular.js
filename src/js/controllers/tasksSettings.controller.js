'use strict';

angular
	.module('TasksApp')
	.controller('TasksSettingsController', ['$scope', TasksSettingsController]);

function TasksSettingsController($scope) {
	$scope.settings = {
		namefilter: '',
		completedfilter: '',
		priorityfilter: '',
		datefilter: ''
	};
}
// this is to expand the project in the future