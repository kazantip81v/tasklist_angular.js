'use strict';

angular
	.module('TasksApp')
	.directive('taskDescr', [taskDescr]);

function taskDescr() {
	return {
		restrict: 'A',
		templateUrl: 'views/taskDescrTemplate.html',
		controller: TaskDescrController,
		scope: {
			items: '='
		},
		link: function(scope, element, attributes) {

			scope.turnOnEditMode = function(items, item) {
				items[items.indexOf(item)].editable = true;
			};

			scope.turnOffEditMode = function(items, item) {
				items[items.indexOf(item)].editable = false;
			};

		}
	}
}