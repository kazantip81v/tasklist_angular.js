'use strict';

angular
    .module('TasksApp')
    .directive('taskForm', [taskForm]);

function taskForm() {
    return {
        restrict: 'A',
        templateUrl: 'views/taskFormTemplate.html',
        controller: TaskFormController,
        scope: {
            items: '='
        },
        link: function(scope, element, attributes) {

        }
    }
}