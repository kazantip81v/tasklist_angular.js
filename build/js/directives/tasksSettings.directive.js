'use strict';

angular
	.module('TasksApp')
	.directive('tasksSettings', tasksSettings);

function tasksSettings() {
	return {
		restrict: 'A',
		templateUrl: 'views/tasksSettingsTemplate.html',
		scope: {
			settings: '='
		}
	}
}