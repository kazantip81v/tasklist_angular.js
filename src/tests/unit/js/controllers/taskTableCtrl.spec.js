describe('Tasks Table Controller', function() {
	var scope,
			rootScope,
			controller,
			dataFactory;

	var item = { "id": 1,
								"name": "Today_task15",
								"creation_date": "2015-04-21T06:50:21",
								"due_date": "2015-04-22T23:59:00",
								"start_date": "2015-04-21T00:00:01"
							};

	beforeEach(function() {
		module('TasksApp', function() {});

		inject(function($rootScope, $controller) {
			rootScope = $rootScope;
			scope = $rootScope.$new();
			controller = $controller;
			
			dataFactory = jasmine.createSpyObj('dataFactory', ['getAllTasks', 'getTask', 'addTask', 'updateTask', 'deleteTask', 'save']);
		});
	});

	it('should call getAllTasks', function() {
		controller('TasksTableController', {$scope: scope, $rootScope: rootScope, 'dataFactory': dataFactory});
		expect(dataFactory.getAllTasks).toHaveBeenCalled();
	});

	it('should call getAllTasks on items:updated event', function() {
		controller('TasksTableController', {$scope: scope, $rootScope: rootScope, 'dataFactory': dataFactory});
		rootScope.$emit('items:updated');
		expect(dataFactory.getAllTasks.calls.count()).toEqual(2);
	});

	it('should call getAllTasks on items:added event', function() {
		controller('TasksTableController', {$scope: scope, $rootScope: rootScope, 'dataFactory': dataFactory});
		rootScope.$emit('items:added');
		expect(dataFactory.getAllTasks.calls.count()).toEqual(2);
	});

	it('should call getAllTasks on items:deleted event', function() {
		controller('TasksTableController', {$scope: scope, $rootScope: rootScope, 'dataFactory': dataFactory});
		rootScope.$emit('items:deleted');
		expect(dataFactory.getAllTasks.calls.count()).toEqual(2);
	});

	it('should call deleteTask', function() {
		controller('TasksTableController', {$scope: scope, $rootScope: rootScope, 'dataFactory': dataFactory});
		scope.delete(item);
		expect(dataFactory.deleteTask).toHaveBeenCalled();
	});
});

