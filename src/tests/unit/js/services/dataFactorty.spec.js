describe('dataFactorty service', function() {
	var $httpBackend, injector;

	var response = [
		{
			"id": 1,
			"name": "Today_task15",
			"creation_date": "2015-04-21T06:50:21",
			"due_date": "2015-04-22T23:59:00",
			"start_date": "2015-04-21T00:00:01"
		},
		{
			"id": 2,
			"name": "Today_task16",
			"creation_date": "2015-04-21T06:50:21",
			"due_date": "2015-04-22T23:59:00",
			"start_date": "2015-04-21T00:00:01"
		}
	];

	var newItem = {
		"id": 3,
		"name": "Upcoming_task",
		"creation_date": "2015-04-22T06:50:23",
		"due_date": "2015-04-29T06:50:29"
	};

	beforeEach(function() {
		module('TasksApp', function() {});

		inject(function($injector) {
			injector = $injector;
			$httpBackend = $injector.get('$httpBackend');
			$httpBackend.when('GET', 'js/tasksData.json').respond(response);
			$httpBackend.when('POST', 'js/tasksData').respond(newItem);
			$httpBackend.when('PUT', 'js/tasksData/2').respond(response[1]);
			$httpBackend.when('PUT', 'js/tasksData/5').respond(response[1]);
			$httpBackend.when('DELETE', 'js/tasksData/2').respond('');
			$httpBackend.when('GET', /views\//).respond(200);
		});
	});

	afterEach(function() {
		$httpBackend.verifyNoOutstandingExpectation();
		$httpBackend.verifyNoOutstandingRequest();
	});

	it('calls tasksData', function() {
		$httpBackend.expectGET('js/tasksData.json');
		injector.get('dataFactory');
		$httpBackend.flush();
	});

	it('returns false for unknown item', function() {
		var service = injector.get('dataFactory');
		$httpBackend.flush();
		expect(service.getTask('43534535')).toBe(null);
	});

	it('returns particular item', function() {
		var service = injector.get('dataFactory');
		$httpBackend.flush();
		expect(service.getTask('1')).toEqual(response[0]);
	})

	it('returns all items', function() {
		var service = injector.get('dataFactory');
		$httpBackend.flush();
		expect(service.getAllTasks()).toEqual(response);
	});

	it('should update items from controller', function() {
		var service = injector.get('dataFactory');
		$httpBackend.flush();
		var allItems = service.getAllTasks();
		var firstItem = service.getTask('1');
		firstItem.name = 'test';
		expect(firstItem).toEqual(allItems[0]);
	});

	it('should add new item', function() {
		var service = injector.get('dataFactory');
		$httpBackend.flush();
		var allItems = service.getAllTasks();
		$httpBackend.expectPOST('js/tasksData');
		service.addTask(newItem);
		$httpBackend.flush();
		expect(service.getAllTasks().length).toEqual(3);
	});

	it('should update item', function() {
		var service = injector.get('dataFactory');
		$httpBackend.flush();
		var secondItem = service.getTask(2);
		secondItem.name = 'test';
		$httpBackend.expectPUT('js/tasksData/2');
		service.updateTask(secondItem);
		$httpBackend.flush();
		expect(service.getTask(2)).toEqual(secondItem);
	});

	it('should delete item', function() {
		var service = injector.get('dataFactory');
		$httpBackend.flush();
		var secondItem = service.getTask(2);
		$httpBackend.expectDELETE('js/tasksData/2');
		service.deleteTask(secondItem);
		$httpBackend.flush();
		expect(service.getTask(2)).toEqual(null);
	});

	it('should add items without id', function() {
		var service = injector.get('dataFactory');
		$httpBackend.flush();
		$httpBackend.expectPOST('js/tasksData');
		service.save({
			'name': 'some title'
		});
		$httpBackend.flush();
	});

	it('should update item with id', function() {
		var service = injector.get('dataFactory');
		$httpBackend.flush();
		$httpBackend.expectPUT('js/tasksData/5');
		service.save({
			'id': 5,
			'name': 'some title'
		});
		$httpBackend.flush();
	});
});