'use strict'; //don't work correctly

angular
	.module('TasksApp')
	.filter('taskSelect', taskSelect);

function taskSelect() {
	var filteredItems = [];
	var i = 0;

	return function (items) {
		
		for (;  i < items.length; i++) {
			if (items[i]._select) {
				filteredItems.push(items[i]);
			}
		}

		return filteredItems;
	};
}