'use strict';

angular
	.module('TasksApp', ['ngAnimate', 'ngRoute'])
	.config(['$routeProvider', '$locationProvider', config]);

function config($routeProvider, $locationProvider) {
	$locationProvider.hashPrefix('');

	$routeProvider.
	when('/tasks', {
		templateUrl: 'views/table.html',
		controller: TasksTableController
	}).
    /*when('/add', {
        templateUrl: 'views/taskForm.html',
        controller: TaskFormController
    }).*/
	when('/edit/:itemId', {
		templateUrl: 'views/teskDescr.html',
		controller: TaskDescrController
	}).
	otherwise({
		redirectTo: '/tasks'
	});
}