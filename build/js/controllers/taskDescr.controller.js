'use strict';

angular
    .module('TasksApp')
    .controller('TaskDescrController', ['$scope', '$rootScope', '$routeParams', '$location', 'dataFactory', TaskDescrController]);

function TaskDescrController($scope, $rootScope, $routeParams, $location, dataFactory) {
    $scope.item = {};

    if ($routeParams.itemId !== undefined) {
        $scope.item = dataFactory.getTask($routeParams.itemId);
        if (undefined === $scope.item || null === $scope.item) {
            //item with this id not found
            $location.path('/add').replace();
        }
    }

    $scope.save = function() {
        dataFactory.save($scope.item);
        $location.path('/tasks').replace();
    };

    $rootScope.$on('item:added', function() {
        $location.path('/tasks').replace();
    });
}