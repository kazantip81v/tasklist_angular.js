describe('Task Descr Controller', function() {
	var scope,
			rootScope,
			routeParams,
			location,
			controller
			dataFactory;

	beforeEach(function() {
		module('TasksApp', function() {});

		inject(function($rootScope, $routeParams, $location, $controller) {
			rootScope = $rootScope;
			scope = $rootScope.$new();
			routeParams = $routeParams;
			location = $location;
			controller = $controller;

			dataFactory = jasmine.createSpyObj('dataFactory', ['getAllTasks', 'getTask', 'addTask', 'updateTask', 'deleteTask', 'save']);
		});
	});

	it('should redirect to /add if item not found', function() {
		routeParams.itemId = 2;
		scope.item = null;
		controller('TaskDescrController', {$scope: scope, $rootScope: rootScope, $routeParams: routeParams, $location: location,'dataFactory': dataFactory});
		expect(location.path()).toBe('/add');
	});

	it('should redirect to /tasks on item:added', function() {
		controller('TaskDescrController', {$scope: scope, $rootScope: rootScope, $routeParams: routeParams, $location: location,'dataFactory': dataFactory});
		rootScope.$emit('item:added', {id: 3});
		expect(location.path()).toBe('/tasks');
	});

	it('should redirect to /tasks & called $scope.save', function() {
		controller('TaskDescrController', {$scope: scope, $rootScope: rootScope, $routeParams: routeParams, $location: location,'dataFactory': dataFactory});
		scope.save();
		expect(dataFactory.save).toHaveBeenCalled();
		expect(location.path()).toBe('/tasks');
	});
});

