'use strict';

angular
	.module('TasksApp')
	.controller('TasksTableController', ['$scope', '$rootScope', 'dataFactory', TasksTableController]);

	function TasksTableController($scope, $rootScope, dataFactory) {
		$scope.items = dataFactory.getAllTasks();

		$rootScope.$on('items:updated', function() {
			$scope.items = dataFactory.getAllTasks();
		});

		$rootScope.$on('items:added', function() {
			$scope.items = dataFactory.getAllTasks();
		});

		$rootScope.$on('items:deleted', function() {
			$scope.items = dataFactory.getAllTasks();
		});

		$scope.delete = function(item) {
			dataFactory.deleteTask(item);
		};
	}