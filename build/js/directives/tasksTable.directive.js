'use strict';

angular
	.module('TasksApp')
	.directive('tasksTable', [tasksTable]);

function tasksTable() {
	return {
		restrict: 'A',
		templateUrl: 'views/tasksTableTemplate.html',
		controller: TasksTableController,
		scope: {
			items: '=',
			settings: '='
		}
	}
}