'use strict';

angular
	.module('TasksApp')
	.factory('dataFactory', ['$http', '$rootScope', '$timeout', dataFactory]);

function dataFactory($http, $rootScope, $timeout) {
	var items = [];
	var service = {};

	getItems();

	function getItems() {
		$http({method: 'GET', url: 'js/tasksData.json'})
			.then(
				function(response) {
					items = response.data;
					$rootScope.$broadcast('items:updated');
					console.log('Yes - successful receipt of the database');
				},
				function(error) {
					console.log(error, "Sorry - database load failed");
				}
			);
	}

	service.getAllTasks = function() {
		return items;
	};

	service.getTask = function(id) {
		var item = null;
		angular.forEach(items, function(value) {
			if (parseInt(value.id) === parseInt(id)) {
				item = value;
				return false;
			}
		});
		return item;
	};

	service.addTask = function(item) {
		$http({method: 'POST', url: 'js/tasksData', data: item})
			.then(
				function(response) {
					items.push(response.data);
					$rootScope.$broadcast('item:added', response.data);
				},
				function(error) {
					$rootScope.$broadcast('item:error', error.data);
				}
			);
	};

	service.updateTask = function(item) {
		$http({method: 'PUT', url: 'js/tasksData/' + item.id, data: item})
			.then(
				function(response) {
					$rootScope.$broadcast('item:updated', response.data);
				},
				function(error) {
					$rootScope.$broadcast('item:error', error.data);
					$timeout( alert("Sorry - sending databases failed"), 7000 );
				}
			);
	};

	service.deleteTask = function(item) {
		$http({method: 'DELETE', url: 'js/tasksData/' + item.id})
			.then(
				function(response) {
					angular.forEach(items, function(value, i) {
						if (parseInt(value.id) === parseInt(item.id)) {
							items.splice(i, 1);
							return false;
						}
					});
					$rootScope.$broadcast('item:deleted', response.data);
				},
				function(error) {
					$rootScope.$broadcast('item:error', error.data);
				}
			);
	};

	service.save = function(item) {
		if (undefined !== item.id && parseInt(item.id) > 0) {
			service.updateTask(item);
		}
		else {
			service.addTask(item);
		}
	};

	return service;
}
