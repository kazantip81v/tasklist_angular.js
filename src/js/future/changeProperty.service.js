/**
 *
 * @object - имя объекта
 * @property - свойство объекта в виде строки 'property'
 * @value - значение свойства (number. string. boolean)
 */

'use strict';

angular
	.module('TasksApp')
	.service('changeProperty', changeProperty);

function changeProperty() {
	return {
		addProperty: function(object, property, value) {
			return object[property] = value;
		},
		removeProperty: function(object, property) {
			delete object[property];
		}
	};
}