'use strict';

angular
	.module('TasksApp')
	.controller('routeController', ['$scope', '$location', routeController]);

function routeController($scope, $location) {
	$scope.$on('$locationChangeSuccess', function () {
		$scope.location = $location.path().replace('/', '');
	});
}
// this is to expand the project in the future, maybe