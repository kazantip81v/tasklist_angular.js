'use strict'; //don't work correctly

angular
	.module('TasksApp')
	.filter('taskStatus', taskStatus);

function taskStatus() {
	var filteredItem = [];
	var i = 0;

	return function (items) {

		for (; i < items.length; i++) {
			if (items[i].obj_status === 'active') {
				filteredItem.push(items[i]);
			}
		}

		return filteredItem;
	};
}